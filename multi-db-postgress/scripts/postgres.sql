DROP TABLE IF EXISTS HEROES;
CREATE TABLE HEROES (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    skill TEXT NULL
);

-- insert
INSERT INTO HEROES (name, skill)
VALUES
    ('Flash', 'Speed'),
    ('Aquaman', 'Aquatic physiology'),
    ('Batman', 'Money')

--select
SELECT * FROM HEROES;
SELECT * FROM HEROES WHERE name = 'Flash';

--update
UPDATE HEROES 
SET name= 'Goku', skill='Super Sayajin'
WHERE id = 1;

--delete
DELETE FROM HEROES 
WHERE id=2;