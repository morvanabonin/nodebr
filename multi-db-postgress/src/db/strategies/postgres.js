const ICrud = require('./interfaces/interfaceCrud');
const Sequelize = require('sequelize');

class Postgres extends ICrud {
    constructor() {
        super();
        this._drive = null;
        this._heroes = null;
        this._conn();
    }

    _conn() {
        this._drive = new Sequelize(
            'heroes',
            'heroes',
            'h3r03s', {
                host: '127.0.0.1',
                port: 5432,
                dialect: 'postgres'
            }
        );
    }

    async isConnected() {
        try {
            await this._drive.authenticate();
            return true;
        } catch (error) {
            console.error(error);
            return false;
        }
    }

    async defineModel() {
        this._heroes = drive.define('heroes', {
            id: {
                type: Sequelize.INTEGER,
                required: true,
                primaryKey: true,
                autoIncrement: true
            },
            name: {
                type: Sequelize.STRING,
                required: true
            },
            skill: {
                type: Sequelize.STRING,
                required: true
            }
        }, 
        {
            tableName: 'heroes',
            freezeTableName: false,
            timestamps: false
        })
        await Heroes.sync();
    }

    create(item) {
        console.log('The item was saved in Postgres database');
    }
}

module.exports = Postgres;