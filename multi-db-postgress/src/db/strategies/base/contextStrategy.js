const ICrud = require('./../interfaces/interfaceCrud');

/**
 * Strategic class create to use multiple databases, extends ICrud interface
 */
class ContextStrategy extends ICrud {

    /**
     * Constructor of ContextStrategy class
     * @param {object} strategy
     */
    constructor(strategy) {
        super();
        this._database = strategy;
    }

    /**
     * method to verify the connection
     */
    isConnected() {
        return this._database.isConnected();
    }

    /**
     * create method
     * @param {string} item
     */
    create(item) {
        return this._database.create(item);
    }

    /**
     * read method
     * @param {string} item
     */
    read(item) {
        return this._database.read(item);
    }

    /**
     * update method
     * @param {int} id
     * @param {string} item
     */
    update(id, item) {
        return this._database.update(id, item);
    }

    /**
     * delete method
     * @param {int} id
     */
    delete(id) {
        return this._database.delete(id);
    }
}

module.exports = ContextStrategy;