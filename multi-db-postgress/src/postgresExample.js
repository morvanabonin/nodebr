// npm install sequelize
// install drives to work with postgres (npm install pg-hstore pg)
const Sequelize = require('sequelize');
const drive = new Sequelize(
    'heroes',
    'heroes',
    'h3r03s',
    {
        host: '172.19.0.3',
        dialect: 'postgres'
    }
);

// when use Promises, use async await
async function main() {
    const Heroes = drive.define('heroes', {
        id: {
            type: Sequelize.INTEGER,
            required: true,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            required: true
        },
        skill: {
            type: Sequelize.STRING,
            required: true
        }
    }, 
    {
        tableName: 'heroes',
        freezeTableName: false,
        timestamps: false
    })
    await Heroes.sync();
    await Heroes.create({
        name: 'Black Canary',
        skill: 'Martial Art'
    });

    let retAllData = await Heroes.findAll({ raw: true });
    console.log('Bring all data', retAllData); 
}

main();