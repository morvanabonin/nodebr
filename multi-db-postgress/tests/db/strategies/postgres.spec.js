const assert = require('assert');
const Postgres = require('../../../src/db/strategies/postgres');
const ContextStrategy = require('../../../src/db/strategies/base/contextStrategy');

const ctx = new ContextStrategy(new Postgres());

describe('Postgres Strategy', function () {
    this.timeout(Infinity);
    it('PostgresSQL connection', async function () {
        const result = await ctx.isConnected();

        assert.equal(result, true);
    });
});

