const Commander = require('commander')
const Database = require('./database')
const Hero = require('./hero')

async function main() {
	Commander
		.version('v1')
		.option('-i, --id [value]', "Id of the hero")
		.option('-n, --name [value]', "Name of the hero")
		.option('-s, --skill [value]', "Skill of the hero")

		.option('-a, --add', "Add a new hero")
		.option('-l, --list', "List the heros")
		.option('-r, --remove', "Remove a hero by id")
		.option('-u, --update [value]', "Update a hero by id")
		.parse(process.argv)

		const hero = new Hero(Commander)

	try {
		if(Commander.add) {
			if( hero.id === undefined) {
				delete hero.id
			}

			const resultAdd = await Database.add(hero)

			if(! resultAdd) {
				console.error('Error to add a hero')
				return;
			}

			console.log('Hero was add with success')
		}

		if(Commander.list) {
			const resultList = await Database.list()

			if(! resultList) {
				console.error('Error to list a hero')
				return;
			}

			console.log(resultList)
		}

		if(Commander.remove) {
			const resultRemove = await Database.remove(hero.id)

			if(! resultRemove) {
				console.error('Error to remove a hero')
				return;
			}

			console.log('Hero was remove with success')
		}

		if(Commander.update) {
			id = parseInt(Commander.update)
			const data = JSON.stringify(hero)
			const dataUpdate = JSON.parse(data)
			const resultUpdate = await Database.update(id, dataUpdate)

			if(! resultUpdate) {
				console.error('Error to update a hero')
				return;
			}

			console.log('Hero was update with success')
		}

	}catch(error) {
		console.error('Error', error)
	}
}

main()