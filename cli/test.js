const {deepStrictEqual, ok} = require('assert')

const database = require('./database')

const DEFAULT_ITEM_ADD = {
    id: 1,
    name: 'Flash',
    skill: 'Speed'
}

const DEFAULT_ITEM_UPDATE = {
    id: 2,
    name: 'Green Lantern',
    skill: 'Ring'
}

describe ('List to manage of the heros', () => {
    before(async () => {
        await database.add(DEFAULT_ITEM_ADD)
        await database.add(DEFAULT_ITEM_UPDATE)
    })
    it('must read a hero using files', async () => {
        const expected = DEFAULT_ITEM_ADD
        const [result] = await database.list(expected.id)

        //ok(result, expected)
        deepStrictEqual(result, expected)
    })

    it('must create a hero, using files', async () => {
        const expected = DEFAULT_ITEM_ADD
        const result = await database.add(DEFAULT_ITEM_ADD)
        const [actual] = await database.list(DEFAULT_ITEM_ADD.id)

        deepStrictEqual(actual, expected)
    })

    it('must delete a hero by id', async () => {
        const expected = true
        const result = await database.remove(DEFAULT_ITEM_ADD.id)
        
        deepStrictEqual(result, expected)
    })
    it('must update a hero by id', async () => {
        const expected = {
            ...DEFAULT_ITEM_UPDATE,
            name: 'Batman',
            skill: 'Money'
        }

        const newData = {
            name: 'Batman',
            skill: 'Money'
        }

        await database.update(DEFAULT_ITEM_UPDATE.id, newData)
        const [result] = await database.list(DEFAULT_ITEM_UPDATE.id)

        deepStrictEqual(result, expected)
    })
})