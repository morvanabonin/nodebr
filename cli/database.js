const { readFile, writeFile } = require('fs');
const { promisify } = require('util')

const readFileAsync = promisify(readFile)
const writeFileAsync = promisify(writeFile)

// another way to get data from json format
// const dataJson = require('./heros.json')

class Database {

    constructor() {
        this.NAME_FILE = 'heros.json';
    }

    async getDataFromFile() {
        const file = await readFileAsync(this.NAME_FILE, 'utf8')
        return JSON.parse(file.toString())
    }

    async writeFile(data) {
        await writeFileAsync(this.NAME_FILE, JSON.stringify(data))
        return true
    }
    
    async add(hero) {
        const data = await this.getDataFromFile()
        const id = hero.id <= 2 ? hero.id : Date.now()

        // this piece of code it is a concatenation between hero, skill and id
        const heroWithId = {
            id,
            ...hero
        }

        const finalData = [
            ...data,
            heroWithId
        ]

        const result = await this.writeFile(finalData)
        return result
    }

    async list(id) {
        const data = await this.getDataFromFile()
        const dataFilter = data.filter(item => (id ? (item.id === id) : true))
    
        return dataFilter
    }

    async remove(id) {
        if(!id) {
            await this.writeFile([])
        }

        const data = await this.getDataFromFile()
        const index = data.findIndex(item => item.id === parseInt(id))

        if(index === -1) {
            throw Error('User not found!')
        }

        data.splice(index, 1)
        console.log(data)
        return await this.writeFile(data)
    }

    async update(id, updateData) {
        const data = await this.getDataFromFile()
        const index = data.findIndex(item => item.id === parseInt(id))

        if(index === -1) {
            throw Error('Hero does not found! Maybe he was not create yet.')
        }

        const actual = data[index]

        const updateObject = {
            ...actual,
            ...updateData
        }
        data.splice(index, 1)

        return await this.writeFile([
            ...data,
            updateObject
        ])
    }
}

module.exports = new Database()