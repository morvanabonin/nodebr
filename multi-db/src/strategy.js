class NotImplementedException extends Error {

    constructor() {
        super("Not Implemented Exception");
    }
}

class ICrud {
    create(item) {
        throw new NotImplementedException();
    }

    read(query) {
        throw new NotImplementedException();
    }

    update(id, item) {
        throw new NotImplementedException();
    }

    delete(id) {
        throw new NotImplementedException();
    }
}

class MongoDB extends ICrud {
    constructor() {
        super();
    }

    create(item) {
        console.log('The item was saved in Mongo database');
    }
}

class Postgres extends ICrud {
    constructor() {
        super();
    }

    create(item) {
        console.log('The item was saved in Postgres database');
    }
}

class ContextStrategy {
    constructor(strategy) {
        this._database = strategy;
    }

    create(item) {
        return this.database.create(item);
    }

    read(item) {
        return this.database.read(item);
    }

    update(id, item) {
        return this.database.update(id, item);
    }

    delete(id) {
        return this.database.delete(id);
    }
}

const contextMongo = new ContextStrategy(new MongoDB());
contextMongo.create();

const contextPostgres = new ContextStrategy(new Postgres());
contextPostgres.create();