class NotImplementedException extends Error {

    constructor() {
        super("Not Implemented Exception");
    }
}

class ICrud {

    isConnected() {
        throw new NotImplementedException();
    }

    /**
     * @param {string} item
     */
    create(item) {
        throw new NotImplementedException();
    }

    /**
     * @param {string} query
     */
    read(query) {
        throw new NotImplementedException();
    }

    /**
     * @param {int} id
     * @param {string} item
     */
    update(id, item) {
        throw new NotImplementedException();
    }

    /**
     * @param {*} id
     */
    delete(id) {
        throw new NotImplementedException();
    }
}

module.exports = ICrud;