const { get } = require('axios')

const URL = 'https://swapi.co/api/people'

async function getPeople(name) {
    try {
        const url = `${URL}/?search=${name}&format=json`
        const result = await get(url)
        // use JSON.stringfy to convert the result into a string and bring all
        // console.log(JSON.stringify(result.data))
        return result.data.results.map(mappingPeople)

    } catch (error) {
        console.error('Service error', error)
    }
}

function mappingPeople(item) {
    return {
        name: item.name,
        height: item.height
    }
}

module.exports = {
    getPeople
}