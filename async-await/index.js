/**
 * Promises
 */

 //import a internal nodejs's module
 const util = require('util')
 const getAddressAsync = util.promisify(getAddress)

function getUser() {
	// if some problem occurs, we call the reject param
	// success, we call the resolve param
	return new Promise(function resolvePromise(resolve, reject) {
		setTimeout(function () {
			return resolve({
				id: 1,
				name: 'Simba',
				bdDay: new Date()
			})
		}, 1000)
	})
}

function getPhone(idUser) {
	return new Promise(function resolvePromise(resolve, reject) {
		setTimeout(() => {
			return resolve({
				phone: '30619317',
				ddd: 51
			})
		}, 2000)
	})
}

function getAddress(idUser, callback) {
	setTimeout(() => {
		return callback (null, {
			street: 'Street of Flowers',
			number: 145
		})
	}, 2000)
}

// 1º step - add the word async - it will return a promise, automatically
async function main() {
	try {
		console.time('promise-spend-time')
		const user = await getUser()
		// const phone = await getPhone(user.id)
		// const address = await getAddressAsync(user.id)
		const result = await Promise.all([
			getPhone(user.id),
			getAddressAsync(user.id)
		])
		const address = result[1]
		const phone = result[0]
		console.log(`
			Name: ${user.name}
			Phone: (${phone.ddd}) ${phone.phone}
			Address: ${address.street},${address.number}
		`)
		console.timeEnd('promise-spend-time')
	} catch (error) {
		console.error("Error", error)
	}
}
main()

// const userPromise = getUser()
// // para manipular o sucesso, usa-se a função .then
// // para manipular erros, usa-se o catch
// // user -> phone ->
// userPromise
// 	.then(function (user) {
// 		return getPhone(user.id)
// 			.then(function getPhone(result) {
// 				return {
// 					user: {
// 						name: user.name,
// 						id: user.id
// 					},
// 					phone : result
// 				}
// 			})
// 	})
// 	.then( function (result) {
// 		const address = getAddressAsync(result.user.id)
// 		return address.then( function resolveAddress(resultAddress) {
// 			return {
// 				user: result.user,
// 				phone: result.phone,
// 				address: resultAddress
// 			}
// 		})
// 	})
// 	// .then(function (result) {
// 	// 	console.log('user result using promise', result)
// 	// })
// 	.then(function (result) {
// 		console.log(`
// 			Name: ${result.user.name}
// 			Address: ${result.address.street},${result.address.number}
// 			Phone: (${result.phone.ddd}) ${result.phone.phone}
// 		`)
// 	})
// 	.catch(function (error) {
// 		console.log('catch error', error)
// 	})