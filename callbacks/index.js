/**
 * 0 - Obeter um usuario
 * 1 - Obter o numero de telefone de um usuario a partir de seu Id
 * 2 - Obter o endereço do usuário pelo Id
 */

function getUser(callback) {
	setTimeout(function () {
		return callback(null, {
			id: 1,
			name: 'Simba',
			bdDay: new Date()
		})
	}, 1000)
}

function getPhone(idUser, callback) {
	setTimeout(() => {
		return callback (null, {
			phone: '30619317',
			ddd: 51
		})
	}, 2000)
}

function getAddress(idUser, callback) {
	setTimeout(() => {
		return callback (null, {
			street: 'Street of Flowers',
			number: 145
		})
	}, 2000)
}

function resolveUser(error, user) {
	console.log('user', user)
}

getUser(function resolveUser(err, user) {
	//null || "" || 0 === false
	if(err) {
		console.log('User with error', err)
		return;
	}

	getPhone(user.id, function resolvePhone(err1, phone) {
		if(err1) {
			console.log('Phone with error', err1)
			return;
		}

		getAddress (user.id, function resolveAdd(err2, address) {
			if(err2) {
				console.log('Address with error', err2)
				return;
			}

			console.log(`
				Name: ${user.name},
				Address: ${address.street},${address.number}
				Phone: (${phone.ddd})${phone.phone}
			`)
		})
	})

})

// const user = getUser()
// const phone = getPhone(user.id)

// console.log('user', user)
// console.log('phone', phone)
