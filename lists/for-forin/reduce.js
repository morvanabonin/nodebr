const { getPeople } = require('./service')

Array.prototype.myReduce = function (callback, initialValue) {
    let finalValue = typeof initialValue !== undefined ? initialValue : this[0]

    for(let index = 0; index <= this.length -1; index ++) {
        finalValue = callback(finalValue, this[index], this)
    }

    return finalValue
}

async function main() {
    try {
        const { results } = await getPeople('a')
        const weight = results.map(item => parseInt(item.height))
        console.log('weight', weight)

        // // [20, 30, 40] = some result =P
        // const total = weight.reduce((prev, next) => {
        //     return prev + next
        // }, 0)

        const myList = [
            ['Simba', 'Nala', 'Mufasa', 'Scar'],
            ['Timon', 'Pumbaa', 'Rafiki', 'Zazu']
        ]

        const total = myList.myReduce((prev, next) => {
            return prev.concat(next)
        }, [])
        .join(', ')

        console.log('total', total)
    } catch (error) {
        console.error('Error ', error)
    }
}

main()