const service = require('./service')

async function main() {
    try {
        const result = await service.getPeople('a')
        const names = []

        console.time('for')
        for(let i = 0; i <= result.results.length - 1; i++) {
            const person = result.results[i]
            names.push(person.name)
        }
        console.timeEnd('for')

        console.time('for-in')
        for (let i in result.results) {
            const person = result.results[i]
            names.push(person.name)
        }
        console.timeEnd('for-in')

        console.time('for-of')
        for(person of result.results) {
            names.push(person.name)
        }
        console.timeEnd('for-of')

        console.log(`names`, names)
    } catch (error) {
        console.log('Error', error)
    }
}

main()