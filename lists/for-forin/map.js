const service = require('./service')

Array.prototype.myMap = function (callback) {
    const newArrayMapping = []

    for (let index = 0; index <= this.length - 1; index++) {
        const result = callback(this[index], index)
        newArrayMapping.push(result)
    }

    return newArrayMapping
}

async function main() {
    try {
        const result = await service.getPeople('a')
        // const names = []

        // result.results.forEach(function (item) {
        //     names.push(item.name)
        // })

        // const names = result.results.map(function (person) {
        //     return person.name
        // })

        // const names = result.results.map((person) => person.name)

        // const names = result.results.myMap(function (person, index) {
        //     return `[${index}]${person.name}`
        // })

        const names = result.results.myMap((person, index) => person.name)

        console.log('names', names)
    } catch (error) {
        console.error('Error')
    }
}
main()