const EventEmitter = require('events')

class MyEmitter extends EventEmitter {

}
const myEmitter = new MyEmitter()
const nameEvent = 'user:click'

myEmitter.on(nameEvent, function (click) {
    console.log('a user clicked', click)
})

// myEmitter.emit(nameEvent, 'in the scroll bar')
// myEmitter.emit(nameEvent, 'at ok')

// let count = 0

// setInterval(function () {
//     myEmitter.emit(nameEvent, 'at ok ' + (count ++))

// }, 1000)

const stdin = process.openStdin()

function main() {
    return new Promise(function( resolve, reject) {
        stdin.addListener('data', function (value) {
            //console.log(`Você digitou:  ${value.toString().trim()}`)
            return resolve(value)
        })
    })
}

main().then(function (result) {
    console.log('result', result.toString())
})